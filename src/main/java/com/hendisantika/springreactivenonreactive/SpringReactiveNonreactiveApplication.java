package com.hendisantika.springreactivenonreactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveNonreactiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringReactiveNonreactiveApplication.class, args);
    }

}
