package com.hendisantika.springreactivenonreactive.repository;

import com.hendisantika.springreactivenonreactive.model.Message;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-reactive-nonreactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/11/19
 * Time: 21.15
 */
@Repository
public interface ReactiveRepository extends ReactiveMongoRepository<Message, String> {

}