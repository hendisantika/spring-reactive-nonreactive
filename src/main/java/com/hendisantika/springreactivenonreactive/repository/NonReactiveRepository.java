package com.hendisantika.springreactivenonreactive.repository;

import com.hendisantika.springreactivenonreactive.model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-reactive-nonreactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/11/19
 * Time: 21.13
 */
@Repository
public interface NonReactiveRepository extends MongoRepository<Message, String> {

}