package com.hendisantika.springreactivenonreactive.controller;

import com.hendisantika.springreactivenonreactive.model.Message;
import com.hendisantika.springreactivenonreactive.repository.ReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-reactive-nonreactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/11/19
 * Time: 21.17
 */

@RestController
public class WebFluxController {

    @Autowired
    ReactiveRepository reactiveRepository;

    @RequestMapping("/webflux/{id}")
    public Mono<Message> findByIdReactive(@PathVariable(value = "id") String id) {
        return reactiveRepository.findById(id);
    }

    @PostMapping("/webflux")
    public Mono<Message> postReactive(@Valid @RequestBody Message message) {
        return reactiveRepository.save(message);
    }

    @DeleteMapping("/webflux")
    public Mono<Void> deleteAllReactive() {
        return reactiveRepository.deleteAll();
    }
}