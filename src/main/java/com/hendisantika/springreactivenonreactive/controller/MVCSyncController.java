package com.hendisantika.springreactivenonreactive.controller;

import com.hendisantika.springreactivenonreactive.model.Message;
import com.hendisantika.springreactivenonreactive.repository.NonReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-reactive-nonreactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/11/19
 * Time: 21.16
 */
@RestController
public class MVCSyncController {

    @Autowired
    NonReactiveRepository nonReactiveRepository;

    @RequestMapping("/mvcsync/{id}")
    public Message findById(@PathVariable(value = "id") String id) {
        return nonReactiveRepository.findById(id).orElse(null);
    }

    @PostMapping("/mvcsync")
    public Message post(@Valid @RequestBody Message message) {
        return nonReactiveRepository.save(message);
    }
}