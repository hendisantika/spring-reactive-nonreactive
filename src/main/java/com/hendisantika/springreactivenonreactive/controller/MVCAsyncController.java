package com.hendisantika.springreactivenonreactive.controller;

import com.hendisantika.springreactivenonreactive.model.Message;
import com.hendisantika.springreactivenonreactive.repository.NonReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.CompletableFuture;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-reactive-nonreactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/11/19
 * Time: 21.16
 */
@RestController
public class MVCAsyncController {

    @Autowired
    NonReactiveRepository nonReactiveRepository;

    @RequestMapping("/mvcasync/{id}")
    public CompletableFuture<Message> findById(@PathVariable(value = "id") String id) {
        return CompletableFuture.supplyAsync(() -> nonReactiveRepository.findById(id).orElse(null));
    }

    @PostMapping("/mvcasync")
    public CompletableFuture<Message> post(@Valid @RequestBody Message message) {
        return CompletableFuture.supplyAsync(() -> nonReactiveRepository.save(message));
    }
}