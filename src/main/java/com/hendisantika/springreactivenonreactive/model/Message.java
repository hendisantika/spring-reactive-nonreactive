package com.hendisantika.springreactivenonreactive.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-reactive-nonreactive
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/11/19
 * Time: 21.12
 */
@Document(collection = "messages")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Message {

    @Id
    private String id;

    @NotBlank
    private String content;

    @NotNull
    private Date createdAt = new Date();

}